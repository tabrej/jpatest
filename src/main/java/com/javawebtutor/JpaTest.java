package com.javawebtutor;

import java.io.IOException;
import java.util.List;
import java.util.function.Predicate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

public class JpaTest {
	private static EntityManager em;

	public static void main(String[] args) throws IOException {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("EmployeePU");
		em = emf.createEntityManager();
		/*
		 * Dept dept=new Dept(); dept.setDeptName("Textile"); dept.setDeptno(4);
		 * dept.setId(4); createEmployee(4, "Tabrej", "Ahamad", dept); dept=new
		 * Dept(); dept.setDeptName("IT"); dept.setDeptno(5); dept.setId(5);
		 * createEmployee(5, "Khan", "bhai", dept); dept=new Dept();
		 * dept.setDeptName("Marketing"); dept.setDeptno(6); dept.setId(6);
		 * createEmployee(6, "Anuj", "Kumar", dept);
		 */

		String deptName = "Textile,Marketing";

		CriteriaBuilder cb = em.getCriteriaBuilder();

		CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);

		Root<Employee> root = cq.from(Employee.class);
		// Predicate whereUser= cb.equal(root.get("").get("deptno"),1);
/*
		CriteriaQuery<Employee> where = cq.select(root).where(cb.equal(root.get("dept").get("deptno"), 1));
		TypedQuery<Employee> query = em.createQuery(cq);
		List<Employee> empList = query.getResultList();
		if (empList != null && empList.size() > 0) {
			for (Employee e : empList) {
				e.getDept().getDeptName();
				System.out.println("First Name:" + e.getFirstName());
				System.out.println("Last Name:" + e.getLastName());
				System.out.println("Dept no:" + e.getDept().getDeptno());
				System.out.println("Dept name:" + e.getDept().getDeptName());
			}
		}*/

		Logger rootLogger = Logger.getRootLogger();
		PatternLayout layout = new PatternLayout("%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n");
	//	RollingFileAppender fileAppender = new RollingFileAppender(layout, System.getProperty("log.file", "./vnt.log"));
		FileAppender fileAppender = new FileAppender(layout, System.getProperty("log.file", "./vnt.log"));

		fileAppender.setImmediateFlush(true);
		fileAppender.setThreshold(Level.INFO);
		fileAppender.setAppend(true);
	//	fileAppender.setMaxFileSize("100MB");
	//	fileAppender.setMaxBackupIndex(1000);

		rootLogger.addAppender(fileAppender); 
		
		for(int i=0;i<2000000;i++){
			rootLogger.info(" hello dear cvcf dfdfd gfgfgf rtrtr fgtrtretr hghg gfgfgfg fgfgf=::"+i);
		}

	}

	private static void createEmployee(int id, String firstName, String lastName, Dept dept) {
		em.getTransaction().begin();
		Employee emp = new Employee(id, firstName, lastName, dept);
		em.persist(emp);
		em.getTransaction().commit();
	}
}
