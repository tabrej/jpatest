FROM openjdk:8
EXPOSE 8080
ADD /.m2/repository/com/javawebtutor/JPAMavenExample/1.0-SNAPSHOT/JPAMavenExample-1.0-SNAPSHOT.jar springboot-dockerapp.jar
ENTRYPOINT ["java","-jar","/springboot-dockerapp.jar"]
